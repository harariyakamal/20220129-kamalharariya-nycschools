//
//  MockedNetworkManager.swift
//  20220129-KamalHarariya-NYCSchoolsTests
//
//  Created by Harariya, Kamal on 1/30/22.
//

import Foundation
@testable import _0220129_KamalHarariya_NYCSchools

class MockedNetworkManager: NetworkManager {
    override func getResponse<T: Codable>(from urlPath: String, completion: @escaping ((Result<T, APIError>) -> Void)) {
        
        var fileName = "SchoolList"
        if urlPath == "f9bf-2cp4.json" {
            fileName = "SchoolDetails"
        }
        
        guard let fileUrl = Bundle(for: type(of: self)).url(forResource: fileName, withExtension: "json") else {
            return completion(.failure(.fileNotFound))
        }

        do {
            let decoder = JSONDecoder()
            decoder.keyDecodingStrategy = .convertFromSnakeCase
            
            let data = try Data(contentsOf: fileUrl)
            let response = try decoder.decode(T.self, from: data)
            completion(.success(response))
        } catch {
            completion(.failure(.commonError))
        }
    }
}

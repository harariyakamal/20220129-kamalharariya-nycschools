//
//  SchoolListViewModelTests.swift
//  SchoolListViewModelTests
//
//  Created by Harariya, Kamal on 1/30/22.
//

import XCTest
@testable import _0220129_KamalHarariya_NYCSchools

class SchoolViewModelTests: XCTestCase {
    
    func testSchoolList() {
        let viewModel = SchoolListViewModel(networkManager: MockedNetworkManager())
        let exp = expectation(description: "wait untill the mocked response is received")
        viewModel.getSchoolList {(result: Result<[School], APIError>) in
            switch result {
            case .success:
                let rows = viewModel.getSchoolCount()
                XCTAssertEqual(rows, 3, "There should be 3 rows in the response")
                exp.fulfill()
            case .failure:
                XCTFail("Test is failed.")
            }
        }
        wait(for: [exp], timeout: 3.0)
    }
    
    func testSchoolDetail() {
        let viewModel = SchoolDetailViewModel(dbn: "02M260", networkManager: MockedNetworkManager())
        let exp = expectation(description: "wait untill the mocked response is received")
        viewModel.getSchoolDetails(completion: {(result: Result<Bool, APIError>) in
            defer { exp.fulfill() }
            switch result {
            case .success(let isSuccess):
                if isSuccess {
                    let sections = viewModel.numberOfSections()
                    XCTAssertEqual(sections, 6, "Should be 6 sections")
                } else {
                    XCTFail("Test is failed.")
                }
            case .failure:
                XCTFail("Test is failed.")
            }
        })
        wait(for: [exp], timeout: 3.0)
    }
    
    func schoolDetailText() {
        let viewModel = SchoolDetailViewModel(dbn: "02M260", networkManager: MockedNetworkManager())
        let exp = expectation(description: "wait untill the mocked response is received")
        viewModel.getSchoolDetails(completion: {(result: Result<Bool, APIError>) in
            defer { exp.fulfill() }
            switch result {
            case .success(let isSuccess):
                if isSuccess {
                    var text = viewModel.getText(at: 0)
                    XCTAssertEqual(text, "02M260", "Should be Same")
                    
                    text = viewModel.getText(at: 1)
                    XCTAssertEqual(text, "UNIVERSITY NEIGHBORHOOD HIGH SCHOOL", "Should be Same")
                } else {
                    XCTFail("Test is failed.")
                }
            case .failure:
                XCTFail("Test is failed.")
            }
        })
        wait(for: [exp], timeout: 3.0)
    }
    
    func testSchoolDetailHeader() {
        let viewModel = SchoolDetailViewModel(dbn: "02M260", networkManager: MockedNetworkManager())
        let exp = expectation(description: "wait untill the mocked response is received")
        viewModel.getSchoolDetails(completion: {(result: Result<Bool, APIError>) in
            defer { exp.fulfill() }
            switch result {
            case .success(let isSuccess):
                if isSuccess {
                    var text = viewModel.getHeaderTitle(section: 0)
                    XCTAssertEqual(text, "DBN", "Should be Same")
                    
                    text = viewModel.getHeaderTitle(section: 1)
                    XCTAssertEqual(text, "School Name", "Should be Same")
                } else {
                    XCTFail("Test is failed.")
                }
            case .failure:
                XCTFail("Test is failed.")
            }
        })
        wait(for: [exp], timeout: 3.0)
    }
}

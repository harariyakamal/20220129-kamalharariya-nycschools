//
//  SchoolDetailViewModel.swift
//  20220129-KamalHarariya-NYCSchools
//
//  Created by Harariya, Kamal on 1/30/22.
//

import Foundation

class SchoolDetailViewModel {
    
    private let networkManager: NetworkManager
    private let dbn: String
    
    private let path = "f9bf-2cp4.json"
    private var schoolDetail: SchoolDetail?
    private var sections = [String]()
    
    init(dbn: String, networkManager: NetworkManager = NetworkManager()) {
        self.dbn = dbn
        self.networkManager = networkManager
    }
    
    func getSchoolDetails(completion: @escaping ((Result<Bool, APIError>) -> Void)) {
        if let _ = CacheManager.shared.cachedSchoolDetails(dbn: dbn) {
            return completion(.success(true))
        }
        
        networkManager.getResponse(from: path) {[weak self] (result: Result<[SchoolDetail], APIError>) in
            guard let self = self else { return }
            switch result {
            case .success(let schoolDetails):
                if CacheManager.shared.schoolDetails.isEmpty {
                    CacheManager.shared.schoolDetails[self.path] = schoolDetails
                }
                let schoolDetail = CacheManager.shared.updateCachedSchoolDetail(dbn: self.dbn, details: schoolDetails)
                self.schoolDetail = schoolDetail
                completion(.success(true))
            case .failure(let error):
                completion(.failure(error))
            }
        }
    }
    
    func numberOfSections() -> Int {
        return self.schoolDetail != nil ? SchoolDetailRow.allCases.count : 0
    }
    
    func getText(at index: Int) -> String {
        if let row = SchoolDetailRow(rawValue: index) {
            switch row {
            case .dbn: return schoolDetail?.dbn ?? ""
            case .schoolName: return schoolDetail?.schoolName ?? ""
            case .testTakers: return schoolDetail?.numOfSatTestTakers ?? ""
            case .writeScore: return schoolDetail?.satWritingAvgScore ?? ""
            case .readingScore: return schoolDetail?.satCriticalReadingAvgScore ?? ""
            case .avgScore: return schoolDetail?.satMathAvgScore ?? ""
            }
        }
        
        return ""   // Returning empty string for default case.
    }
    
    func getHeaderTitle(section: Int) -> String? {
        let row = SchoolDetailRow(rawValue: section)
        return row?.displayString
    }
}

//
//  SchoolDetailViewController.swift
//  20220129-KamalHarariya-NYCSchools
//
//  Created by Harariya, Kamal on 1/30/22.
//

import UIKit

class SchoolDetailViewController: UIViewController {
    private var tableView: UITableView!
    private var viewModel: SchoolDetailViewModel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUIElements()
        getSchoolDetails()
    }
    
    private func setupUIElements() {
        view.backgroundColor = .white
        
        tableView = UITableView(frame: .zero, style: .insetGrouped)
        view.addSubview(tableView)
        tableView.dataSource = self
        tableView.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            tableView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            tableView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            tableView.topAnchor.constraint(equalTo: view.topAnchor),
            tableView.bottomAnchor.constraint(equalTo: view.bottomAnchor)
        ])
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: "SchoolDetailViewCellID")
        tableView.cellLayoutMarginsFollowReadableWidth = true
    }
    
    private func getSchoolDetails() {
        viewModel.getSchoolDetails(completion: {[weak self] (result: Result<Bool, APIError>) in
            guard let self = self else { return }
            switch result {
            case .success(let isSuccess):
                if isSuccess {
                    self.reloadView()
                } else {
                    // Handler errors here.
                }
            case .failure: break // Handler errors here.
            }
        })
    }
    
    private func reloadView() {
        if self.viewModel.numberOfSections() == 0 {
            let alertController = UIAlertController(title: "Alert!", message: "There are no scores for this selected School, Hence not reloading the list view.", preferredStyle: .alert)
            let action = UIAlertAction(title: "Okay", style: .default, handler: {[weak self] _ in
                guard let self = self else { return }
                self.dismiss(animated: true, completion: nil)
                self.navigationController?.popViewController(animated: true)
            })
            alertController.addAction(action)
            present(alertController, animated: true, completion: nil)
        } else {
            self.tableView.reloadData()
        }
    }
    
    static func create(with viewModel: SchoolDetailViewModel) -> SchoolDetailViewController {
        let vc = SchoolDetailViewController()
        vc.viewModel = viewModel
        return vc 
    }
}

extension SchoolDetailViewController: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return viewModel.numberOfSections()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SchoolDetailViewCellID", for: indexPath)
        cell.textLabel?.text = viewModel.getText(at: indexPath.section)
        cell.textLabel?.numberOfLines = 0
        cell.textLabel?.font = .preferredFont(forTextStyle: .body)
        return cell
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return viewModel.getHeaderTitle(section: section)
    }
}


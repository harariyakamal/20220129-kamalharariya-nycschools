//
//  SchoolDetail.swift
//  20220129-KamalHarariya-NYCSchools
//
//  Created by Harariya, Kamal on 1/30/22.
//

import Foundation

// Assuming that these fields are required and will have value always because it is mentioned in the requirements that I am supposed to filter based on dbn and use scores.
struct SchoolDetail: Codable {
    let dbn: String
    let schoolName: String
    let numOfSatTestTakers: String
    let satCriticalReadingAvgScore: String
    let satMathAvgScore: String
    let satWritingAvgScore: String
}

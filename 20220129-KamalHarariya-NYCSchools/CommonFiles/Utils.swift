//
//  Utils.swift
//  20220129-KamalHarariya-NYCSchools
//
//  Created by Harariya, Kamal on 1/30/22.
//

import Foundation

enum APIError: Error {
    case commonError // Would be good if we can provide more cases here.
    case fileNotFound
}

enum SchoolDetailRow: Int, CaseIterable {
    case dbn
    case schoolName
    case testTakers
    case writeScore
    case readingScore
    case avgScore
    
    var displayString: String {
        switch self {
        case .dbn: return "DBN"
        case .schoolName: return "School Name"
        case .testTakers: return "Test Takers"
        case .writeScore: return "SAT Writing Score"
        case .readingScore: return "SAT Reading Score"
        case .avgScore: return "SAT Average Score"
        }
    }
}

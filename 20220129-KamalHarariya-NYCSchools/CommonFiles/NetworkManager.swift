//
//  NetworkManager.swift
//  20220129-KamalHarariya-NYCSchools
//
//  Created by Harariya, Kamal on 1/30/22.
//

import Foundation

let baseUrl = "https://data.cityofnewyork.us/resource/"

class NetworkManager {
    
    private let sharedSession = URLSession.shared
    private let decoder = JSONDecoder()
    
    init() {
        decoder.keyDecodingStrategy = .convertFromSnakeCase
    }
        
    func getResponse<T: Codable>(from urlPath: String, completion: @escaping ((Result<T, APIError>) -> Void)) {
        if let cachedResponse = CacheManager.shared.schoolDetails[urlPath] as? T {
            return completion(.success(cachedResponse))
        }
        
        let urlString = baseUrl + urlPath
        guard let url = URL(string: urlString) else { return completion(.failure(.commonError)) }
        let request = URLRequest(url: url)
        
        sharedSession.dataTask(with: request) {[weak self] data, response, error in
            DispatchQueue.main.async {
                guard let self = self else { return completion(.failure(.commonError)) }
                if let data = data, let parsedResponse = try? self.decoder.decode(T.self, from: data) {
                    completion(.success(parsedResponse))
                } else {
                    completion(.failure(.commonError))
                }
            }
        }.resume()
    }
}

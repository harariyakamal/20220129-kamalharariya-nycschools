//
//  CacheManager.swift
//  20220129-KamalHarariya-NYCSchools
//
//  Created by Harariya, Kamal on 1/30/22.
//

import Foundation

class CacheManager {
    static var shared = CacheManager()  // Using singleton instance so that I can share the same information all over the application.
    
    private init() { } // Just to avoid no one in the app is able to initiate the instance of `CacheManager`.
    
    // Map to store the selected information
    private var selectedSchoolDetails = [String: SchoolDetail]()
    
    var schoolDetails: [String: [SchoolDetail]] = [:]
    
    func cachedSchoolDetails(dbn: String) -> SchoolDetail? {
        return selectedSchoolDetails[dbn]
    }
    
    func updateCachedSchoolDetail(dbn: String, details: [SchoolDetail]) -> SchoolDetail? {
        if let schoolDetail = details.filter({$0.dbn == dbn }).first { // Assuming that the dbn will be the primary key and will be available always. Will show an alert in case the records aint found.
            selectedSchoolDetails[dbn] = schoolDetail
            
            return schoolDetail
        }
        
        return nil
    }
}

//
//  SchoolListViewModel.swift
//  20220129-KamalHarariya-NYCSchools
//
//  Created by Harariya, Kamal on 1/30/22.
//

import Foundation

class SchoolListViewModel {
    
    private let networkManager: NetworkManager
    private let schoolListPath = "s3k6-pzi2.json"
    
    private var schools = [School]()
    
    init(networkManager: NetworkManager = NetworkManager()) {
        self.networkManager = networkManager
    }
    
    func getSchoolList(completion: @escaping ((Result<[School], APIError>) -> Void)) {
        networkManager.getResponse(from: schoolListPath) {[weak self] (result: Result<[School], APIError>) in
            guard let self = self else { return }
            switch result {
            case .success(let schools):
                self.schools = schools
                completion(.success(schools))
            case .failure(let error):
                completion(.failure(error))
            }
        }
    }
    
    func numberOfRows() -> Int {
        return schools.count
    }
    
    func getSchool(at index: Int) -> School? {
        if schools.count > index {
            return schools[index]
        }
        
        return nil 
    }
    
    // This is just for unit tests.
    func getSchoolCount() -> Int {
        return schools.count
    }
}

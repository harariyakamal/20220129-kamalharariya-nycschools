//
//  School.swift
//  20220129-KamalHarariya-NYCSchools
//
//  Created by Harariya, Kamal on 1/30/22.
//

import Foundation

struct School: Codable {
    // Making these fields optional so it can be handled properly when we dont get these fields from server.
    let dbn: String?
    let schoolName: String?
    let overviewParagraph: String?
    let city: String?
    let boro: String?
}

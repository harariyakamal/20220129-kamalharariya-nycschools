//
//  SchoolListTableViewCell.swift
//  20220129-KamalHarariya-NYCSchools
//
//  Created by Harariya, Kamal on 1/30/22.
//

import UIKit

class SchoolListTableViewCell: UITableViewCell {
    @IBOutlet weak var schoolName: UILabel!
    @IBOutlet weak var boroInitials: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        boroInitials.clipsToBounds = true
        boroInitials.layer.cornerRadius = 30.0 // this is hard coded value.
        boroInitials.backgroundColor = .lightGray
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
    }
}

extension SchoolListTableViewCell {
    func updateContents(school: School) {
        schoolName.text = school.schoolName // Not sure what do I do when the school name is empty. Showing empty Label doesn't make any sense here. In documentation, I couldn't find that confirms that the school name will not be nil. 
        boroInitials.text = school.boro ?? "#"  // Using a default character to avoid showing empty label.
    }
}

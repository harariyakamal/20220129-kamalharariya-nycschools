//
//  SchoolListViewController.swift
//  20220129-KamalHarariya-NYCSchools
//
//  Created by Harariya, Kamal on 1/30/22.
//

import UIKit

class SchoolListViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    let viewModel = SchoolListViewModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.cellLayoutMarginsFollowReadableWidth = true
        getListOfSchools()
    }
    
    private func getListOfSchools() {
        viewModel.getSchoolList {[weak self] (result: Result<[School], APIError>) in
            guard let self = self else { return }
            switch result {
            case .success:
                self.tableView.reloadData()
            case .failure: break // Handler errors here.
            }
        }
    }
}

extension SchoolListViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.numberOfRows()
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "SchoolCellID", for: indexPath) as? SchoolListTableViewCell else { return UITableViewCell() }
        if let school = viewModel.getSchool(at: indexPath.row) {
            cell.updateContents(school: school)
        }
        return cell
    }
    
    // School selection
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        // Can use storyboard easily here.
        let selectedDBN = viewModel.getSchool(at: indexPath.row)?.dbn ?? ""
        let vm = SchoolDetailViewModel(dbn: selectedDBN)
        let detailVC = SchoolDetailViewController.create(with: vm)
        navigationController?.pushViewController(detailVC, animated: true)
        tableView.deselectRow(at: indexPath, animated: true)
    }
}
